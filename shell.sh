#!/bin/bash
#sleep 15

echo '1. creating virtualenv'
virtualenv --python=/usr/bin/python2.7 venvflask
echo '2. activate'
source venvflask/bin/activate
echo '3. install requerimnts'
pip install -r src/requirements.txt
echo '4. PYTEST'
pytest &
echo '5. DEPLOY----------------------------------------------------runing main.py'
python src/main.py &
echo ' construyendo docker image'
docker build -t yavinenana/miuflask:1.0 --file Dockerfile .
docker push yavinenana/miuflask:1.0
docker rmi yavinenana/miuflask:1.0
rm -rf $WORKSPACE
echo '----------------------------------finish-----------------------------------'
