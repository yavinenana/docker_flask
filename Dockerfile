FROM ubuntu:16.04
EXPOSE 5000
RUN apt-get update 
RUN apt-get install python python-pip -y 
RUN pip install --upgrade pip
RUN mkdir /opt/app/

COPY src/* /opt/app/
#COPY src/requirements.txt /opt/app/requirements.txt

RUN pip install -r /opt/app/requirements.txt 

#ENTRYPOINT ["pip install -r", "requirements.txt" ]
ENTRYPOINT ["python", "/opt/app/main.py"]
#RUN sleep 15
#ENTRYPOINT "/docker-entrypoint.sh"
